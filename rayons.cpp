﻿/******************************************************************/
/*                                                                */
/* noms + matricules                                              */
/*                                                                */
/* Marianne Brisson 19094436                                      */
/*                                                                */
/* Marie-Helene Gadbois Del Carpio 19091796                       */
/*                                                                */
/*                                                                */
/*                                                                */
/*                                                                */
/*                                                                */
/******************************************************************/






#include "rayons.h"
#include "couleurs.h"
#include "attr.h"
#include "ensemble.h"
#include "point.h"
#include "transfo.h"
#include "inter.h"
#include "vision.h"
#include "util.h"
#include "utilalg.h"
#include <stdio.h>
#include <math.h>


Couleur calculer_intensite_pt_inter(Objet* scene, const Camera& camera, vecteur dir, point pt_inter, vecteur& vn, Couleurs& c);
Couleur calculer_intensite_rayon(point O, vecteur dir, Objet* scene, const Camera& camera);

void Enregistre_pixel(int no_x, int no_y, Couleur intens, Fichier f)
// enregistre la couleur du pixel dans le fichier f
// Chacune des composantes de la couleur doit etre de 0 a 1,
// sinon elle sera ramene a une borne.
{

	reel r, v, b;
	char c;

	intens = intens * 255.0;
	r = intens.rouge();
	if (r < 0) r = 0;
	if (r > 255)r = 255;
	c = (unsigned char)r;
	f.Wcarac(c);

	v = intens.vert();
	if (v < 0) v = 0;
	if (v > 255)
		v = 255;
	c = (unsigned char)v;
	f.Wcarac(c);

	b = intens.bleu();
	if (b < 0) b = 0;
	if (b > 255) b = 255;
	c = (unsigned char)b;
	f.Wcarac(c);

}


booleen TraceRayons(const Camera& camera, Objet* scene, const entier& res, char fichier[])
// Genere un rayon pour chacun des pixel et calcul l'intensite de chacun
{
	Couleur Intensite(0.0, 0.0, 0.0);

	const entier nb_pixel_x = res;
	const entier nb_pixel_y = res;

	Transformation transfInv = Vision_Normee(camera).inverse(); // transformation de vision inverse


	//Ouverture du fichier pour y enregistrer le trace de rayon
	Fichier f;
	if (!f.Open(fichier, "wb")) return FAUX;

	f.Wchaine("P6\r");
	f.Wentier(res); f.Wcarac(' '); f.Wentier(res);	f.Wchaine("\r");
	f.Wentier(255);	f.Wchaine("\r");

	printf("\nDebut du trace de rayons\n");
	printf("%4d source(s) lumineuse(s)\n", camera.NbLumiere());
	
	point origine = camera.PO();

	for (int no_y = 1; no_y <= nb_pixel_y; no_y++)
	{
		for (int no_x = 1; no_x <= nb_pixel_x; no_x++)
		{
			point Pm = point(-(2.0 * (no_x - 1.0) / res) + 1.0, -(2.0 * (no_y - 1.0) / res) + 1.0, 1.0);
			point pointPixel = transfInv.transforme(Pm);
			vecteur dir = pointPixel - origine;
			Intensite = calculer_intensite_rayon(origine, dir, scene, camera);

			Enregistre_pixel(no_x, no_y, Intensite, f);
		}

		//Imprime le # de ligne rendu
		if (no_y % 15 == 0) printf("\n");
		printf("%3d ", no_y);
	}
	printf("\n\nFin du trace de rayons.\n");


	f.Close();
	return VRAI;
}

Couleur calculer_intensite_rayon(point O, vecteur dir, Objet* scene, const Camera& camera) {

	vecteur vn;
	reel k = reel();
	Couleurs c = Couleurs();

	if (Objet_Inter(*scene, O, dir.unitaire(), &k, &vn, &c))
	{
		point pt_inter = O + dir.unitaire() * k;
		return calculer_intensite_pt_inter(scene, camera, dir, pt_inter, vn, c);
	}
	else
	{
		return NOIR;
	}
}

Couleur calculer_intensite_pt_inter(Objet* scene, const Camera& camera, vecteur dir, point pt_inter, vecteur& vn, Couleurs& c)
{
	vecteur	O = -dir.unitaire();
	vecteur ombreVec = vecteur();

	Couleur Intensite = NOIR;
	Couleur Im;
	Couleurs ombreCouleurs = Couleurs();

	int nbLumieres = camera.NbLumiere();

	reel distLumiere = reel();
	reel ombreDist = reel();

	if (O * vn < 0)
	{
		vn = -vn;
	}

	for (int i = 0; i < nbLumieres; i++)
	{
		vecteur L = vecteur(pt_inter, camera.Position(i)).unitaire();
		distLumiere = vecteur(pt_inter, camera.Position(i)).norme();

		//reflexion diffuse 
		Couleur intensiteLumiereDiffuse = camera.Diffuse(i);
		Couleur coeffDiffuse = c.diffus();
		//float theta = acos(vn * L) * 180 / PI;
		float costheta = vn * L;
		Couleur intensiteDiffuse = intensiteLumiereDiffuse * coeffDiffuse * costheta;

		//reflexion speculaire
		vecteur H = L + O;
		H.normalise();
		float cosalpha = vn * H;
		if (cosalpha < 0) {
			cosalpha = 0;
		}
		if (costheta < 0) {
			cosalpha = 0;
		}
		Couleur coeffSpeculaire = c.speculaire();
		Couleur intensiteSpec = intensiteLumiereDiffuse * coeffSpeculaire * pow(cosalpha, 90.0);

		//lumiere ambiante
		Couleur intensiteLumiereAmbiante = camera.Ambiante(i);
		Couleur coeffAmbiant = c.ambiant();
		Couleur intensiteAmbiante = intensiteLumiereAmbiante * coeffAmbiant*coeffDiffuse;

		//calculer illumination et ombre
		if (Objet_Inter(*scene, pt_inter, L, &ombreDist, &ombreVec, &ombreCouleurs)) {
			if (ombreDist < distLumiere) {
				Intensite = Intensite + intensiteAmbiante;
			}
			else {
				Intensite = Intensite + intensiteDiffuse + intensiteSpec + intensiteAmbiante;
			}
		}
		else {
			Intensite = Intensite + intensiteDiffuse + intensiteSpec + intensiteAmbiante;
		}
		//reflexions miroirs
		if (c.reflechi() != NOIR)
		{
			vecteur RO = -vecteur(2 * (vn * dir) * vn - dir);//reflechi de O
			Im = calculer_intensite_rayon(pt_inter, RO, scene, camera);
			Intensite = Intensite + (c.reflechi() * Im);
		}
	}
	
	return Intensite;
}